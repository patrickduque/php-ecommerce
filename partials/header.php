<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="../views/home.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/add_category.php">Add category</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/add_product.php">Add Product</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/catalog.php">Catalog</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/cart.php">Cart</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/login.php">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/register.php">Register</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../controllers/logout.php">Logout</a>
      </li>
    </ul>
  </div>
</nav>
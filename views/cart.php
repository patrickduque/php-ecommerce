<?php
    require_once '../partials/template.php';
    function get_content(){ ?>
        <div class="container my-4">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Cart Page</h2>
                </div>
            </div>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="cart-items">
                    <thead>
                        <tr>
                            <th>items</th>
                            <th>price</th>
                            <th>qty</th>
                            <th>subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>item name</td>
                            <td>item price</td>
                            <td>item qty</td>
                            <td>item total</td>
                            <td>
                                <form action="" method="POST">
                                    <button class="btn btn-danger m-0">X</button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
<?php } ?>

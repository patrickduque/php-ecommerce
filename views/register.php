<?php
    require_once './../partials/template.php';

    function get_content(){ ?>
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h3 class="display-4 text-center mb-5">Registration Form</h3>
                    <form action="" method="POST" id="registrationForm" class="my-3">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <div class="form-group">
                                    <label for="firstname">Firstname: </label>
                                    <input type="text" name="firstname" id="firstname" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="lastname">Lastname: </label>
                                    <input type="text" name="lastname" id="lastname" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email: </label>
                                    <input type="email" name="email" id="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password: </label>
                                    <input type="password" name="password" id="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="cpassword">Confirm Password: </label>
                                    <input type="password" name="cpassword" id="cpassword" class="form-control">
                                </div>
                                <button class="btn btn-success w-100" type="submit">Register</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

<?php } ?>

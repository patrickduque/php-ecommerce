<?php
    require_once '../partials/template.php';
    function get_content(){ ?>
        <div class="container">
            <div class="row my-5">
                <div class="col-sm-10 col-md-8 mx-auto">
                    <h3 class="display-4 text-center mb-5">Add Product</h3>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="product-name">Product Name: </label>
                            <input type="text" name="product-name" id="product-name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="product-price">Product Price: </label>
                            <input type="text" name="product-price" id="product-price" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="product-image">Product Image: </label>
                            <input type="file" name="product-image" id="product-image" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="product-description">Product Description: </label>
                            <textarea class="form-control" name="product-description" id="product-description" cols="20" rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="product-category">Product Category: </label>
                            <select name="product-category" id="product-category" class="form-control">
                                <option value="#">Option1</option>
                                <option value="#">Option1</option>
                                <option value="#">Option1</option>
                                <option value="#">Option1</option>
                            </select>
                        </div>
                        <button class="btn btn-success w-100" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
<?php } ?>

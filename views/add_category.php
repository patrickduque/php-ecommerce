<?php
    require_once '../partials/template.php';
    function get_content(){ ?>
        <div class="container">
            <div class="row my-5">
                <div class="col-sm-10 col-md-8 mx-auto">
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="name">Category Name: </label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                        <button class="btn btn-success w-100" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
<?php }; ?>

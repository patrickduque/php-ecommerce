<?php
    require_once './../partials/template.php';

    function get_content(){ ?>
        <div class="container">
            <div class="row text-center my-3">
                <div class="col-md-4 offset-md-4">
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="username">Enter Username:</label>
                            <input type="text" class="form-control" id="username" name="username">
                        </div>
                        <div class="form-group">
                            <label for="password">Enter Password:</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <button class="btn btn-success w-100" type="submit">Log In</button>
                    </form>
                </div>
            </div>
        </div>
<?php }; ?>
